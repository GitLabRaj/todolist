# Assignment Springboot App todo List (WEB &amp; REST API) , Spring Boot , JPA &amp; H2 database
* Todo list (task) with CURD opeeration
* WEB (HTML) & REST API are implemented
* Swagger API Definition also included. 
* Using JDK 17 (LTS)
* Using Method reference & Optional features.
* API &amp; Web app endpoint for filter the completed & inComplet task also added.


# Web App URL
* URL        : http://localhost:8080/todo/dashboard
* Validation : Validation are added
* Exception  : Handling the Exception


# REST API (SWAGGER UI for Test it)
* URL    : http://localhost:8080/swagger-ui/index.html#/
* Validation : Validation are added
* Exception  : Handling the Exception


# Note
* Security    : Tried to add the JWT security with user & role. But because of the time limit. I skipped it. 
* UI          : Using the very basic UI and not using any additional Script / style sheet
* Plan        : If it is a rea-time applicaton, surely I will make it as very profession application. 




