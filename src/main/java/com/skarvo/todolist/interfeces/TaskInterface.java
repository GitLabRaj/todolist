package com.skarvo.todolist.interfeces;

import com.skarvo.todolist.entity.TodoTask;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * @author SelvaRaj
 */

public interface TaskInterface {

    public ResponseEntity<TodoTask> saveTask(String name) throws Exception;

    public ResponseEntity<TodoTask> updateTask(long id, boolean isCompleted) throws Exception;

    public ResponseEntity<HttpStatus> deleteAllTask();

    public ResponseEntity<HttpStatus> deleteTaskById(long taskId);

    public ResponseEntity<List<TodoTask>> getAllTask() throws Exception;

    public ResponseEntity<TodoTask> findTaskById(long taskId) throws Exception;

    public ResponseEntity<List<TodoTask>> findTaskByIsCompleted(boolean status) throws Exception;
}
