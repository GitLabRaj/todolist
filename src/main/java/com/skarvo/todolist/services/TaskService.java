package com.skarvo.todolist.services;

import com.skarvo.todolist.entity.TodoTask;
import com.skarvo.todolist.exceptions.TaskNotFoundException;
import com.skarvo.todolist.interfeces.TaskInterface;
import com.skarvo.todolist.jpa.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author SelvaRaj
 *
 */

@Service
public class TaskService implements TaskInterface {

    @Autowired
    TaskRepository taskRepository;

    @Override
    public ResponseEntity<List<TodoTask>> getAllTask() throws Exception {
        try {
            List<TodoTask> todoTask = new ArrayList<TodoTask>();
            taskRepository.findAll().forEach(todoTask::add);
            if (todoTask.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(todoTask, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<TodoTask> saveTask(String name) throws Exception {
        try {
            TodoTask _todoTask = taskRepository.save((new TodoTask(name)));
            return new ResponseEntity<>(_todoTask, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<TodoTask> updateTask(long id, boolean isCompleted) throws Exception {
        Optional<TodoTask> taskData = taskRepository.findById(id);
        if (taskData.isPresent()) {
            TodoTask _todoTask = taskData.get();
            _todoTask.setCompleted(isCompleted);
            return new ResponseEntity<>(taskRepository.save(_todoTask), HttpStatus.OK);
        } else {
            throw new TaskNotFoundException(("Task with Id : "+id+" Not Found"));
        }
    }

    @Override
    public ResponseEntity<HttpStatus> deleteAllTask() {
        try {
            taskRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<HttpStatus> deleteTaskById(long taskId) {
        try {
            taskRepository.deleteById(taskId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<TodoTask> findTaskById(long taskId) throws Exception {
        Optional<TodoTask> todoTask = taskRepository.findById(taskId);
        if (todoTask.isPresent()) {
            return new ResponseEntity<>(todoTask.get(), HttpStatus.OK);
        } else {
            throw new TaskNotFoundException(("Task with Id : "+taskId+" Not Found"));
        }
    }

    @Override
    public ResponseEntity<List<TodoTask>> findTaskByIsCompleted(boolean isCompleted) throws Exception {
        try {
            List<TodoTask> todoTask = taskRepository.findByIsCompleted(isCompleted);
            if (todoTask.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(todoTask, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
