package com.skarvo.todolist.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;

/**
 * @author SelvaRaj
 */

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "TASK", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
public class TodoTask implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TASK_ID", unique = true, nullable = false)
    private Long taskId;

    @Column(name = "name")
    private String name;

    @Column(name = "isCompleted")
    private boolean isCompleted;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private Date createdAt;

    @UpdateTimestamp
    @Column(nullable = false, updatable = true)
    private Date updatedAt;

    public TodoTask(String name) {
        this.name = name;
    }

    public TodoTask(String name, boolean isCompleted) {
        this.name = name;
        this.isCompleted = isCompleted;
    }

    public TodoTask(Long taskId, String name, boolean isCompleted) {
        this.taskId = taskId;
        this.name = name;
        this.isCompleted = isCompleted;
    }
}
