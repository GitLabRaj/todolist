package com.skarvo.todolist.jpa;

import com.skarvo.todolist.entity.TodoTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author SelvaRaj
 */

@Repository
public interface TaskRepository extends JpaRepository<TodoTask, Long> {

    List<TodoTask> findByIsCompleted(Boolean isCompleted);
}
