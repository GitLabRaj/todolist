package com.skarvo.todolist.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;

/**
 * @author SelvaRaj
 */

@Configuration
public class OpenAPIConfiguration {

    @Value("${swagger.definition.title}")
    private String swaggerTitle;

    @Value("${swagger.definition.description}")
    private String description;

    @Value("${swagger.definition.version}")
    private String version;

    @Value("${swagger.contact.name}")
    private String contactName;

    @Value("${swagger.contact.site}")
    private String site;

    @Value("${swagger.contact.email}")
    private String email;

    @Value("${skarvo.openapi.dev-url}")
    private String devUrl;

    @Value("${skarvo.openapi.prod-url}")
    private String prodUrl;

    @Bean
    public OpenAPI myOpenAPI() {
        Server devServer = new Server();
        devServer.setUrl(devUrl);
        devServer.setDescription("Development Env");

        Server prodServer = new Server();
        prodServer.setUrl(prodUrl);
        prodServer.setDescription("Production Env");

        Contact contact = new Contact();
        contact.setEmail(email);
        contact.setName(swaggerTitle);
        contact.setUrl(site);

        License mitLicense = new License().name("License").url(site);

        Info info = new Info()
                .title(contactName)
                .version("1.0")
                .contact(contact)
                .description(description).termsOfService(site)
                .license(mitLicense);

        return new OpenAPI().info(info).servers(List.of(devServer, prodServer));
    }
}
