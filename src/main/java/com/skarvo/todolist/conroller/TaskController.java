package com.skarvo.todolist.conroller;

import com.skarvo.todolist.entity.TodoTask;
import com.skarvo.todolist.exceptions.TaskNotFoundException;
import com.skarvo.todolist.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * @author SelvaRaj
 */

@Controller
@CrossOrigin(origins = "http://localhost:8080")
@ComponentScan("com.skarvo.todolist.*")
@RequestMapping(value = "/todo")
public class TaskController {
    @Autowired
    TaskService taskService;

    @RequestMapping(method = RequestMethod.GET, value = "/dashboard")
    public String dashboard(@RequestParam(value = "message", required = false) String message, Model model) {
        // TODO Auto-generated method stub
        List<TodoTask> todoTasks = null;
        try {
            todoTasks = taskService.getAllTask().getBody();
            model.addAttribute("todoTasks", todoTasks);
            model.addAttribute("message", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "dashBoard";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/deleteById")
    public String deleteTaskById(@RequestParam Long taskId, RedirectAttributes attributes) {
        try {
            taskService.deleteTaskById(taskId);
            attributes.addAttribute("message", "Todo item with Id : '" + taskId + "' is removed successfully!");
        } catch (TaskNotFoundException e) {
            e.printStackTrace();
            attributes.addAttribute("message", e.getMessage());
        }
        return "redirect:dashboard";

    }

    @RequestMapping(method = RequestMethod.GET, value = "/edit")
    public String editTodoItem(Model model, RedirectAttributes attributes, @RequestParam Long taskId) {
        String page = null;
        try {
            TodoTask todoTask = taskService.findTaskById(taskId).getBody();
            model.addAttribute("todoTask", todoTask);
            page = "upadteTodoItem";
        } catch (TaskNotFoundException e) {
            e.printStackTrace();
            attributes.addAttribute("message", e.getMessage());
            page = "redirect:dashboard";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return page;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateTask(@ModelAttribute TodoTask todoTask, RedirectAttributes attributes) throws Exception {
        taskService.updateTask(todoTask.getTaskId(), todoTask.isCompleted());
        Long taskId = todoTask.getTaskId();
        attributes.addAttribute("message", "Todo Item with id: '" + taskId + "' is updated successfully !");
        return "redirect:dashboard";
    }

    @RequestMapping(value = "/createTodoItem", method = RequestMethod.GET)
    public String createTodoItem() {
        return "createTodoItem";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveTask(@ModelAttribute TodoTask todoTask, Model model) throws Exception {
        // TODO Auto-generated method stub
        TodoTask todoTaskNew = taskService.saveTask(todoTask.getName()).getBody();
        String message = "Record with id : '" + todoTaskNew.getTaskId() + "' is saved successfully !";
        model.addAttribute("message", message);
        return "redirect:dashboard";
    }
}
