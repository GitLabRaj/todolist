package com.skarvo.todolist.conroller;

import com.skarvo.todolist.entity.TodoTask;
import com.skarvo.todolist.services.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.*;

/**
 *
 * @author SelvaRaj
 *
 */

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@ComponentScan("com.skarvo.todolist.*")
@RequestMapping(value = "/task")
@Tag(name = "TODO List", description = "Assignment Spring Boot todo list API")
@Validated
public class TaskRestController {
    @Autowired
    TaskService taskService;

    @Operation(summary = "Retrieve the all the todo task list.")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TodoTask>> getAllTask() throws Exception {
        // TODO Auto-generated method stub
        return  taskService.getAllTask();
    }

    @Operation(summary = "Retrieve details of particular task by passing his taskId.")
    @RequestMapping(method = RequestMethod.POST, value = "/byid")
    public ResponseEntity<TodoTask> getTaskById(@RequestParam @Min(1) long taskId ) throws Exception {
        // TODO Auto-generated method stub
        return taskService.findTaskById(taskId);
    }

    @Operation(summary = "Save new Task.")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<TodoTask> saveTask(@RequestParam("name") @NotBlank String name) throws Exception {
        // TODO Auto-generated method stub
        return taskService.saveTask(name);
    }

    @Operation(summary = "Update existing Task.")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<TodoTask> updateTask(@RequestParam("taskId") @Min(1) long id, @RequestParam("isCompleted") boolean isCompleted) throws Exception {
        return taskService.updateTask(id,isCompleted);
    }


    @Operation(summary = "Delete task by using taskId.")
    @RequestMapping(method = RequestMethod.DELETE, value = "/delete")
    public ResponseEntity<HttpStatus> deleteAllTask() {
        return taskService.deleteAllTask();
    }

    @Operation(summary = "Delete task by using taskId.")
    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteById")
    public ResponseEntity<HttpStatus> deleteTaskById(@RequestParam("taskId") @Min(1) long taskId) {
        return taskService.deleteTaskById(taskId);
    }

    @Operation(summary = "Todo list based on completed status.")
    @RequestMapping(method = RequestMethod.GET, value = "/completedStatus")
    public ResponseEntity<List<TodoTask>> findByStatus(@RequestParam("isCompleted") boolean isCompleted) throws Exception {
        return taskService.findTaskByIsCompleted(isCompleted);
    }
}
